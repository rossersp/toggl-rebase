const request = require('request-promise-native');

const baseUri = "https://www.toggl.com/api/v8"
const reportsUri = "https://www.toggl.com/reports/api/v2"
const creds = {
  username: 'dee14165fe8d92e57d2d99bf7bc41284',
  password: 'api_token'
}
const perPage = 10;
module.exports = exports = {
  me: function() {
    return request({
      method: 'GET',
      url: `${baseUri}/me`,
      auth: creds,
      json: true
    });
  },
  workspaces: function() {
    return request({
      method: 'GET',
      url: `${baseUri}/workspaces`,
      auth: creds,
      json: true
    })

  },
  listing: async function(workspaceId, from, to) {
    let data = [];
    let page = 1;
    let initialResponse = await request({
      method: 'GET',
      url: `${reportsUri}/details?workspace_id=${workspaceId}&since=${from.format('YYYY-MM-DD')}&until=${to.format('YYYY-MM-DD')}&page=${page}&user_agent=rosshinkley@gmail.com`,
      auth: creds,
      json: true
    });
    data = [...data, ...initialResponse.data];
    if (initialResponse.total_count > perPage) {
      for (page; page <= initialResponse.total_count / perPage; page++) {
        response = await request({
          method: 'GET',
          url: `${reportsUri}/details?workspace_id=${workspaceId}&since=${from.format('YYYY-MM-DD')}&until=${to.format('YYYY-MM-DD')}&page=${page}&user_agent=rosshinkley@gmail.com`,
          auth: creds,
          json: true
        });

        data = [...data, ...response.data];
      }
    }
    return data;
  },
  updateTimeEntry: function(entry) {
    console.dir(entry);
    console.log(`${baseUri}/time_entries/${entry.id}`);
    return request({
      method: 'PUT',
      auth: creds,
      url: `${baseUri}/time_entries/${entry.id}`,
      body: {
        time_entry: entry
      },
      json: true
    });
  }
};