const api = require('./toggl-api');
const util = require('util');
const moment = require('moment');
const fs = require('fs');
const path = require('path');

var prog = require('commander')

prog.version('0.0.0')
  .option('-x, --export', "export your time entries")
  .option('-i, --import', "import your time entries")
  .parse(process.argv);

const entries = async function() {
  let workspaces = await api.workspaces();
  if (workspaces.length > 1) {
    throw new Error('more than one workspace found');
  }
  let workspace = workspaces[0];

  let entries = await api.listing(workspace.id, moment()
    .subtract(0, 'days'), moment());
  return entries.map((entry) => `${entry.id}|${entry.start}|${entry.end}|${entry.dur/1000} ${entry.description}`);
}


const run = async function() {
  if (prog.export && prog.import) {
    throw new Error('import and export at the same time? while snowflame would be proud.... no.');
  } else if (prog.export) {
    (async function() {
      fs.writeFileSync(path.resolve(__dirname, 'edit.txt'), (await entries())
        .join('\n'));
    })();
  } else if (prog.import) {
    let workspaces = await api.workspaces();
    if (workspaces.length > 1) {
      throw new Error('more than one workspace found');
    }
    let workspace = workspaces[0];
    try {
      var results = await Promise.all(fs.readFileSync(path.resolve(__dirname, 'edit.txt'))
        .toString()
        .split('\n')
        .filter((record) => !/^\s*$/.test(record))
        .map(function(record) {
          let r = /(.+?)\s+(.+)/.exec(record);
          var header = r[1].split('|');
          var comment = r[2];
          return {
            id: parseInt(header[0]),
            wid: workspace.id,
            start: header[1],
            stop: header[2],
            duration: header[3],
            description: comment
          };
        })
        .map((entry) => {
          return api.updateTimeEntry(entry);
        }));
    } catch (e) {
      console.dir(e)
    }
    console.dir(results);
  }
};

run();
